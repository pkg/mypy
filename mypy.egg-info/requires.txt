typing_extensions>=4.6.0
mypy_extensions>=1.0.0

[:python_version < "3.11"]
tomli>=1.1.0

[dmypy]
psutil>=4.0

[faster-cache]
orjson

[install-types]
pip

[mypyc]
setuptools>=50

[python2]

[reports]
lxml
